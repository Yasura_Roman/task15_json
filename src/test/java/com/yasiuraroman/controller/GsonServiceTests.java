package com.yasiuraroman.controller;

import com.yasiuraroman.model.*;
import com.yasiuraroman.util.GsonService;
import junit.framework.TestCase;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class GsonServiceTests {

    @Test
    public void jsonConvert(){
        Candie candie = new Candie();
        candie.setName("111");
        candie.setEnergy(22);
        candie.setType(CandieType.CARAMEL);
        candie.setIngredients(Arrays.asList( new Ingredient[]{
                new Ingredient("firstI",1),
                new Ingredient("secondI",2),
                new Ingredient("TheerdI",3)
        }));
        candie.setValues(new Nutrient(10,15,20));
        candie.setProduction(new Production("name","addrs","222222222222"));
        candie.setWeight(12);

        LinkedList<Candie> list = new LinkedList<>();
        list.add(candie);

        GsonService service = new GsonService();
        service.convertCandieToJSON(list);
        String jsonString = service.convertCandieToJSON(list);
        System.out.println();
        Candie newCandie = service.convertJSONToCandie(jsonString).get(0);
        System.out.println(newCandie);
        TestCase.assertEquals(candie.getName(),newCandie.getName());
        TestCase.assertEquals(candie.getEnergy(),newCandie.getEnergy());
        TestCase.assertEquals(candie.getWeight(),newCandie.getWeight());
    }

}
