package com.yasiuraroman.util;

public class KeyDescriptionPair {
    private int key;
    private String description;

    public int getKey() {
        return key;
    }

    public String getDescription() {
        return description;
    }

}

