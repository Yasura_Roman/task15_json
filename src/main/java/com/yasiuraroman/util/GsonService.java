package com.yasiuraroman.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.reflect.TypeToken;
import com.yasiuraroman.model.Candie;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public class GsonService {
    private Gson gson;

    public GsonService() {
        gson = new GsonBuilder().registerTypeAdapter(LocalDate.class,
                (JsonDeserializer<LocalDate>) (json, type, jsonDeserializationContext) -> LocalDate.parse(json.getAsJsonPrimitive().getAsString())).create();
    }
    public String convertCandieToJSON(List<Candie> candies) {
        return gson.toJson(candies);
    }

    public List<Candie> convertJSONToCandie(String json) {
        Type candiesListType = new TypeToken<List<Candie>>(){}.getType();
        return gson.fromJson(json, candiesListType);
    }

    public Map<String, KeyDescriptionPair> convertJSONToKeyDescriptionPair(String json) {
        Type collectionType = new TypeToken<Map<String, KeyDescriptionPair>>(){}.getType();
        return gson.fromJson(json, collectionType);
    }

    public void validate(String jsonPath) throws FileNotFoundException {
        JSONObject jsonSchema =
                new JSONObject(
                        new JSONTokener(
                                new FileInputStream(".\\src\\main\\resources\\myCandiesJSONShema.xml")));
        JSONObject jsonSubject =
                new JSONObject(
                        new JSONTokener(
                                new FileInputStream(jsonPath)));
        Schema schema = SchemaLoader.load(jsonSchema);
        try {
            schema.validate(jsonSubject);
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }
}
