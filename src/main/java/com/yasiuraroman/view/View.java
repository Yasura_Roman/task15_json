package com.yasiuraroman.view;

import com.yasiuraroman.controller.JSONTaskController;
import com.yasiuraroman.controller.JSONTaskControllerImp;
import com.yasiuraroman.model.Candie;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.*;

public class View {
    private Logger logger = LogManager.getLogger();
    private Scanner input = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private JSONTaskController controller;
    private ResourceBundle menuBundle = ResourceBundle.getBundle("Menu");

    public View() {
        controller = new JSONTaskControllerImp();
        createMenu();
        initMenuCommands();
    }

    private void createMenu(){
        menu = new LinkedHashMap<>();
        SortedSet<String> sortedSet = new TreeSet<>();
        sortedSet.addAll(menuBundle.keySet());
        for (String key : sortedSet
        ) {
            menu.put(key,menuBundle.getString(key));
        }
    }

    //-------------------------------------------------------------------------
    private void initMenuCommands() {
        methodsMenu = new HashMap<>();
        methodsMenu.put("1", this::showGSONDataBind);
        methodsMenu.put("2", this::showSort);
    }

    private void showGSONDataBind() {
        try {
            List<Candie> candies = controller.convertJSONToCandie(".\\src\\main\\resources\\myCandiesJSON.json");
            for (Candie candie: candies
                 ) {
                logger.info(candie);
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    private void showSort() {
        try {
            controller.showSortExample(".\\src\\main\\resources\\sorted.json");
            List<Candie> candies = controller.convertJSONToCandie(".\\src\\main\\resources\\sorted.json");

            for (Candie candie: candies
            ) {
                logger.info(candie);
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }
    //-------------------------------------------------------------------------


    private void outputMenu() {
        logger.info("MENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
