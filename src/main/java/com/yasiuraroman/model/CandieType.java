package com.yasiuraroman.model;

public enum CandieType {
    CARAMEL,
    CHOCOLATE,
    GUMMY,
    LICORICE,
    LOLLIPOPS;
}
