package com.yasiuraroman.controller;

import com.yasiuraroman.model.Candie;
import com.yasiuraroman.util.GsonService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class JSONTaskControllerImp implements JSONTaskController {

    private GsonService gsonService = new GsonService();

    @Override
    public List<Candie> convertJSONToCandie(String filePath) throws IOException {
        String json = new String(Files.readAllBytes(Paths.get(filePath)));
        return gsonService.convertJSONToCandie(json);
    }

    @Override
    public void showSortExample(String filePath) throws IOException {
        List<Candie> candies = this.convertJSONToCandie(filePath);
        candies.sort(Candie.getNameComparator());
        String json = gsonService.convertCandieToJSON(candies);
        Files.write(Paths.get(".\\src\\main\\resources\\sorted.json"), json.getBytes());
    }
}
