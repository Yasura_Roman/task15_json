package com.yasiuraroman.controller;

import com.yasiuraroman.model.Candie;

import java.io.IOException;
import java.util.List;

public interface JSONTaskController {
    List<Candie> convertJSONToCandie(String filePath) throws IOException;
    void showSortExample(String filePath) throws IOException;
}
